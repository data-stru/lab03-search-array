/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package sqrt;


/**
 *
 * @author informatics
 */
import java.util.Scanner;
public class Sqrt {

    public static void main(String[] args) {
        int x,r;
        
        Scanner sc = new Scanner(System.in);
        System.out.print("Input: x = ");
        x = sc.nextInt();
        r=x;
        while(r*r >x) r = ((r+x/r)/2) | 0;
        System.out.println("Output: "+r);  

    }
        
}
